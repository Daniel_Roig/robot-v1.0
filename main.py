
import cwiid, time, sys, math, os

import RPi.GPIO as GPIO
from time import sleep
import wheels


#variables globals
wiimote=0;

#declracion GPIO LED
led = 32
#-----------------------------

def wiimoteConnection():

	global wiimote ## I will use gloabel varibale

	print 'Pulsa las teclas 1+2 en el Wiimote...'

	while wiimote==0:

	  try:
	    print "Conectando..."
	    GPIO.output(led,GPIO.HIGH)

	    wiimote = cwiid.Wiimote()


	  except RuntimeError, msg:
	    #print "Error: ",msg
	    print "Conexion fallida"

	    GPIO.output(led,GPIO.HIGH)
	    sleep(0.5)
	    GPIO.output(led,GPIO.LOW)
	    sleep(0.5)
	    GPIO.output(led,GPIO.HIGH)
	    sleep(0.5)
	    GPIO.output(led,GPIO.LOW)
	    sleep(2)

	    print "Intentando de nuevo"

#------------------ Conexion establecida -----------------

	GPIO.output(led,GPIO.LOW)

	return;

class proximity_sensor:

  def __init__(self, pin1, pin2):
      self.p1=pin1
      self.p2=pin2
      GPIO.setup(self.p1, GPIO.OUT)
	  # echo (cable verde en el prototipo)
      GPIO.setup(self.p2, GPIO.IN)

  def distance(self):
      start = 0
      end = 0
      # Configura el sensor
      GPIO.output(self.p1, False)


	 # Empezamos a medir
      GPIO.output(self.p1, True)
      time.sleep(10*10**-6) #10 microsegundos
      GPIO.output(self.p1, False)

      timeout_start = time.time()

      while (time.time() < timeout_start + 0.5) and (GPIO.input(self.p2) == GPIO.LOW):
		  start = time.time()
		  print 'aqui'


	 # Flanco de 1 a 0 = fin
      while GPIO.input(self.p2) == GPIO.HIGH:
		  end = time.time()

      distancia = (end-start) * 343 / 2
      print ("Distancia al objeto =", str(distancia))
      return distancia





GPIO.setup(led,GPIO.OUT)
sleep(10)
wiimoteConnection()




#-- Encender el led 1 wiimote
wiimote.rumble = 1
sleep(0.5)
wiimote.rumble = 0
wiimote.led = cwiid.LED1_ON

#-- Activar los botones y acelerometros
wiimote.rpt_mode = cwiid.RPT_ACC | cwiid.RPT_BTN

sensor=proximity_sensor(11,13)

while True:

    buttons = wiimote.state['buttons']

#-- Leer el calibrado
    ext_type = wiimote.state['ext_type']
    cal = wiimote.get_acc_cal(ext_type)


    if buttons == cwiid.BTN_DOWN:
      print 'Button DOWN pressed'
      wheels.ruedas_dercha()

    if(buttons & cwiid.BTN_UP):
      print 'Button UP pressed'
      wheels.ruedas_izquierda()


    if buttons == cwiid.BTN_1 and 0.3<sensor.distance():
      print 'Button 1 pressed'
      wheels.ruedas_adelante()


    if buttons == cwiid.BTN_2:
      print 'Button 2 pressed'
      wheels.ruedas_atras()


    if (buttons & cwiid.BTN_A):
      print 'Button A pressed'


    if (buttons & cwiid.BTN_B):
      print 'Button B pressed'




    if (buttons - cwiid.BTN_PLUS - cwiid.BTN_MINUS == 0):
      print '\nCerrando conexion...'

      wiimote.rumble = 1
      sleep(0.1)
      wiimote.rumble = 0

      ##GPIO.cleanup()
      wiimote.close();

      ##os.system("sudo shutdown")
      ##sys.exit(0)


    if buttons == 0 or (buttons==cwiid.BTN_1 and 0.3>sensor.distance()):
      wheels.stop()

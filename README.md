# Robot v0.1 #

### First steps [1][2] ###

1. Install Pixel on RP
2. Acces to RP through SSH with ethernet cable
	1. Enable SSH service on Raspberry Pi Configuration
	2. Share your internet connection with the RP
	3. In a Linux or Mac terminal login using "ssh pi@192.168.2.2" and "raspberry" as password
3. Update the RP with "sudo apt-get update"
4. Upgrade the RP with "sudo apt-get upgrade"
5. Assign static Wifi IP to use RP without ethernet cable
	1. Acces "dhcpcd.conf" file with "sudo nano /etc/dhcpcd.conf"
	2. Add at the end
		interface wlan0
		static ip_address=192.168.0.200/24
		static routers=192.168.0.1
		static domain_name_servers=192.168.0.1
	3. Now you can acces to the RP using as login ina terminal "ssh pi@192.168.0.200" an "raspberry" as password

### Make motors work [3] ###
1. Connect motors as image
![](img/robot_schema_bb.jpg)


### Link motors with Wiimote ###
1. Install package "sudo apt-get install python-cwiid"


### Automatic startup [4] ###

### Proximity sensor [5] ###







### Sources ###

[1]Set up RP:
https://stackoverflow.com/questions/41318597/ssh-connection-refused-on-raspberry-pi

https://mycyberuniverse.com/mac-os/connect-to-raspberry-pi-from-a-mac-using-ethernet.html

[2]Static Wifi IP
http://www.lowefamily.com.au/2015/12/08/how-to-configure-a-static-ip-address-on-raspberry-pi/

[3]Motor
https://business.tutsplus.com/tutorials/controlling-dc-motors-using-python-with-a-raspberry-pi--cms-20051

[4]Automatic startup
https://nideaderedes.urlansoft.com/2013/12/20/como-ejecutar-un-programa-automaticamente-al-arrancar-la-raspberry-pi/

[5]Proximity sensor
https://robologs.net/2015/07/31/tutorial-de-raspberry-pi-y-hc-sr04/

http://pitando.australiando.es/2016/02/18/controla-un-sensor-de-proximidad-desde-tu-raspberry-pi/


LED resistor calculator
https://www.kitronik.co.uk/blog/led-resistor-value-calculator/



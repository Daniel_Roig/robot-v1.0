import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD) 

#declaracion izquierda
MotorIzqP = 22
MotorIzqN = 18
MotorEI=16


#declracion derecha
MotorDerP = 19
MotorDerN = 21
MotorED = 23

#declracion GPIO LED
led = 32

#declaracion del pin como salida
GPIO.setup(MotorIzqP,GPIO.OUT) 
GPIO.setup(MotorIzqN,GPIO.OUT)
GPIO.setup(MotorEI,GPIO.OUT)

GPIO.setup(MotorDerP,GPIO.OUT)
GPIO.setup(MotorDerN,GPIO.OUT)
GPIO.setup(MotorED,GPIO.OUT)




def ruedas_izquierda():

	GPIO.output(MotorIzqP,GPIO.LOW)
	GPIO.output(MotorIzqN,GPIO.HIGH)
	GPIO.output(MotorEI,GPIO.HIGH)

	GPIO.output(MotorDerP,GPIO.HIGH)
	GPIO.output(MotorDerN,GPIO.LOW)
	GPIO.output(MotorED,GPIO.HIGH)
	return;
	

def ruedas_dercha():

	GPIO.output(MotorIzqP,GPIO.HIGH)
	GPIO.output(MotorIzqN,GPIO.LOW)
	GPIO.output(MotorEI,GPIO.HIGH)

	GPIO.output(MotorDerP,GPIO.LOW)
	GPIO.output(MotorDerN,GPIO.HIGH)
	GPIO.output(MotorED,GPIO.HIGH)
	return;

	
def ruedas_adelante():

	GPIO.output(MotorIzqP,GPIO.LOW)
	GPIO.output(MotorIzqN,GPIO.HIGH)
	GPIO.output(MotorEI,GPIO.HIGH)

	GPIO.output(MotorDerP,GPIO.LOW)
	GPIO.output(MotorDerN,GPIO.HIGH)
	GPIO.output(MotorED,GPIO.HIGH)
	return;

def ruedas_atras():

	GPIO.output(MotorIzqP,GPIO.HIGH)
	GPIO.output(MotorIzqN,GPIO.LOW)
	GPIO.output(MotorEI,GPIO.HIGH)

	GPIO.output(MotorDerP,GPIO.HIGH)
	GPIO.output(MotorDerN,GPIO.LOW)
	GPIO.output(MotorED,GPIO.HIGH)
	return;

def stop():
	GPIO.output(MotorED,GPIO.LOW)
	GPIO.output(MotorEI,GPIO.LOW)
	return;
